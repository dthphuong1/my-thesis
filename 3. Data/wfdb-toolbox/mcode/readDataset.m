function readDataset(name, s, d)
    [~,config]=wfdbloadlib;
    echo on
    display('Reading samples ECG signal from MIT-BIH Arrhythmia Database . . .')
    [tm,ecg]=rdsamp(strcat('mitdb/', name),s,d);
    
    display('Saving ECG signal to File . . . ')
    fileID = fopen('ecgSignal.txt', 'w');
    for i=1:numel(ecg)
        fprintf(fileID, '%f %f\n', tm(i), ecg(i));
    end
    fclose(fileID);
    display('Saved ECG signal to File completed !')
    
    display('Reading and plotting annotations (human labels) of QRS complexes performend on the signals . . .')
    [ann,type,subtype,chan,num]=rdann(strcat('mitdb/', name),'atr',s,d);
    
    display('Saving annotations (human labels) of QRS complexes performend on the signal to File . . . ')
    fileID = fopen('ecgLabel.txt', 'w');
    for i=1:numel(ann)
        fprintf(fileID, '%d %c %d %d %d\n', ann(i), type(i), subtype(i), chan(i), num(i));
    end
    fclose(fileID);
    display('Saved annotations (human labels) of QRS complexes performend on the signal to File completed ! ')
    
    %Plot 2D version of signal and labels
     figure
     plot(tm(1:d-s+1),ecg(1:d-s+1));hold on;grid on;
     plot(tm(ann(ann<d-s+1)+1),ecg(ann(ann<d-s+1)+1),'ro');
end