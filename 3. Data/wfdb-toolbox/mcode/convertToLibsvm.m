function convertToLibsvm(datasetName, f,t)
    % Read dataset
    [~,config]=wfdbloadlib;
    echo on
    display('Reading samples ECG signal from MIT-BIH Arrhythmia Database . . .')
    fileName = strcat('mitdb/', datasetName);
    [tm,ecg]=rdsamp(fileName,f,t);

    display('=> Saving ECG signal to File . . . ')
    fileID = fopen('save\ecgSignal.txt', 'w');
    for i=1:t-f+1
        fprintf(fileID, '%f %f\n', tm(i), ecg(i));
    end
    fclose(fileID);
    display('=> Saved ECG signal to File completed !')
    
    display('Reading and plotting annotations (human labels) of QRS complexes performend on the signals . . .')
    [ann,type,subtype,chan,num]=rdann(fileName,'atr',1,t);
    
    display('=> Saving annotations (human labels) of QRS complexes performend on the signal to File . . . ')
    fileID = fopen('save\ecgLabel.txt', 'w');
    for i=1:numel(ann)
        fprintf(fileID, '%d %c %d %d %d\n', ann(i), type(i), subtype(i), chan(i), num(i));
    end
    fclose(fileID);
    display('=> Saved annotations (human labels) of QRS complexes performend on the signal to File completed ! ')
    
    num = numel(type);
    lb = zeros(num,1);
    k = 1;
    for i=1:num
        if (type(i) == '+') continue; end
        if (type(i) == 'N') 
            lb(k) = 1; 
            k=k+1; 
        else
            lb(k) = -1; 
            k=k+1;
        end
    end
    
    display('Saving ECG to LIBSVM file . . .')
    fileName = strcat('save\ecg', datasetName, '.libsvm');
    fileID = fopen(fileName, 'w');
    s = 1; d = 289; i = 1;
    while (d <= t - f + 1)
        %figure; plot(tm(s:d), ecg(s:d));
        %title(lb(i));
        
        % save to  libsvm type
        fprintf(fileID, '%d ', lb(i));
        for j = 1:289
            fprintf(fileID, '%d:%f ', j, ecg(s + j)); 
        end
        fprintf(fileID, '\n');
        
        % increase
        i = i + 1;
        s = d;
        d = d + 289;
    end
    fclose(fileID);
    display('Saved LIBSVM file completed ! ')
end