"""
Implementation of k-Nearest Neighbors
"""

import numpy as np
from pylearn.utils import pdist

def knn(X, Y, k=4, dist_type="cosine", task=None, X_label=None):
    s = -pdist(X, Y, dist_type)
    idx = np.argsort(s, axis=1)[:, :-(k+1):-1]
    if task is None:
        return idx
    elif task == "classification":
        assert(X_label is not None)
        label = X_label[idx]
        N = label.shape[0]
        Y_label = [0] * N
        for i in xrange(N):
            l, c = np.unique(label[i, :], return_counts=True)
            Y_label[i] = l[np.argmax(c)]
        return Y_label
    elif task == "classification_probability":
        return -1
    else:
        print "Task %s is not supported yet." % task