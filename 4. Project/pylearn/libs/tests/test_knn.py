import os
import platform

import numpy as np
from pylearn.libs import knn
from sklearn.datasets import load_svmlight_file
from sklearn.metrics import accuracy_score

if platform.system() == "Windows":
    DATA_DIR = "X:/tund/rdata/mnist"
elif platform.system() == "Linux":
    DATA_DIR = "/home/tund/rdata/mnist"
elif platform.system() == "Darwin":
    DATA_DIR = "/Users/tund/rdata/mnist"
else:
    print "Cannot recognize platform."

RAND_SEED = 6789

if __name__ == '__main__':
    X = np.random.rand(10, 4)
    X_label = np.random.randint(3, size=10)
    Y = np.random.rand(5, 4)
    print knn(X, Y, k=4, task="classification", X_label=X_label)

    X_train, y_train = load_svmlight_file(os.path.join(DATA_DIR, "mnist"), n_features=784)
    X_test, y_test = load_svmlight_file(os.path.join(DATA_DIR, "mnist.t"), n_features=784)

    X_train = X_train.toarray() / 255.0
    X_test = X_test.toarray() / 255.0

    y_pred = knn(X_train, X_test, k=4, task="classification", X_label=y_train)

    # 97.36% (2.64% error)
    print "Accuracy = %.4f" % accuracy_score(y_test, y_pred)

