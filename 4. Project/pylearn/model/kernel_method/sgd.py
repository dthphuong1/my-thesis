"""Kernel Stochastic Gradient Descent
"""

from __future__ import division

import time
import numpy as np
from sklearn.base import BaseEstimator
from sklearn.metrics import accuracy_score, mean_squared_error

LOSS = {"hinge": 1, "l1": 2, "l2": 3, "logit": 4, "eps_intensive": 5}
TASK = {"classification": 1, "regression": 2}
KERNEL = {"gaussian": 1}


class KSGD(BaseEstimator):

    def __init__(self, loss="hinge", eps=0.1,
                 kernel="gaussian", gamma=0.1, T = 100,
                 lbd=1.0, avg_weight=False, verbose=0):
        self.loss = loss
        self.eps = eps
        self.kernel = kernel
        self.gamma = gamma
        self.lbd = lbd
        self.T = T
        self.avg_weight = avg_weight
        self.verbose = verbose

    def init(self):
        try:
            self.loss = LOSS[self.loss]
        except KeyError:
            raise ValueError("Loss function %s is not supported." % self.loss)

        try:
            self.kernel = KERNEL[self.kernel]
        except KeyError:
            raise ValueError("Kernel %s is not supported." % self.kernel)

        if self.loss == LOSS["hinge"] or self.loss == LOSS["logit"]:
            self.task_ = TASK["classification"]
        else:
            self.task_ = TASK["regression"]

        self.n_classes_ = 0
        self.class_name_ = None
        self.w_ = None
        self.idw_ = None
        self.X_ = None
        self.train_time_ = 0

    def get_wx(self, t, x):
        if t == 0:
            return [0]
        else:
            if self.kernel == KERNEL["gaussian"]:
                xx = (self.X_[:t, :] - x)
                return np.sum(self.w_[:t, :]*np.exp(-self.gamma*np.sum(xx*xx, axis=1, keepdims=True)), axis=0)
            else:
                return [0]

    def get_wxy(self, t, x, y):
        if t == 0:
            return (0, -1)
        else:
            if self.kernel == KERNEL["gaussian"]:
                xx = (self.X_[:t, :]-x)
                k = np.sum(self.w_[:t, :]*np.exp(-self.gamma*np.sum(xx*xx, axis=1, keepdims=True)), axis=0)
                idx = np.ones(self.n_classes_, np.bool)
                idx[y] = False
                z = np.argmax(k[idx])
                z += (z >= y)
                return (k[y] - k[z], z)
            else:
                return (0, -1)

    def get_grad(self, t, x, y):
        if self.n_classes_ > 2:
            wxy, z = self.get_wxy(t, x, y)
            if self.loss == LOSS["hinge"]:
                return (-1, z) if wxy <= 1 else (0, z)
            else:
                return (-1 / (1 + np.exp(wxy)), z)
        else:
            wx = self.get_wx(t, x)[0]
            if self.loss == LOSS["hinge"]:
                return (-y, -1) if y*wx <= 1 else (0, -1)
            elif self.loss == LOSS["l1"]:
                return (np.sign(wx - y), -1)
            elif self.loss == LOSS["l2"]:
                return (y*(y*wx - 1), -1)
            elif self.loss == LOSS["logit"]:
                return (-y*np.exp(-y*wx) / (np.exp(-y*wx) + 1), -1)
            elif self.loss == LOSS["eps_intensive"]:
                return (np.sign(wx - y), -1) if np.abs(y - wx) > self.eps else (0, -1)

    def fit(self, X, y):
        start_time = time.time()

        self.init()

        N = X.shape[0]
        if self.task_ == TASK["classification"]:
            self.class_name_, y = np.unique(y, return_inverse=True)
            self.n_classes_ = len(self.class_name_)
            if self.n_classes_ == 2:
                y[y == 0] = -1

        if self.n_classes_ > 2:
            self.w_ = np.zeros((N, self.n_classes_))
        else:
            self.w_ = np.zeros((N, 1))
            self.idw_ = np.ones((N,1)) * (-1)

        if self.avg_weight:
            w_avg = np.zeros(self.w_.shape)

        self.X_ = X
        for t in xrange(self.T):
            it = np.random.randint(0, N, 1)

            alpha_it, z = self.get_grad(it, X[it], y[it])
            alpha_it = alpha_it/(self.lbd*(t+1))
            self.w_ *= (1.0*t)/(t+1)
            if it not in self.idw_: #New vector
                self.w_[it] = -alpha_it
                self.idw_[it] = it
            else:
                self.w_[it] += -alpha_it

            if self.avg_weight:
                w_avg += self.w_

        if self.avg_weight:
            self.w_ = w_avg / N

        self.train_time_ = time.time() - start_time

    def predict(self, X):
        y = np.zeros(X.shape[0])
        for i in xrange(X.shape[0]):
            wx = self.get_wx(len(self.w_), X[i])
            if self.task_ == TASK["classification"]:
                if self.n_classes_ == 2:
                    y[i] = self.class_name_[wx[0] >= 0]
                else:
                    y[i] = self.class_name_[np.argmax(wx)]
            else:
                y[i] = wx[0]
        return y

    def score(self, X, y):
        score = accuracy_score if self.task_ == TASK["classification"] else mean_squared_error
        return float(score(self.predict(X), y))
