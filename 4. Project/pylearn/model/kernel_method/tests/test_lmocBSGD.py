from pylearn.model.kernel_method.lmocBSGD import lmocBSGD

import time
import numpy as np
from sklearn.datasets import load_svmlight_file
from sklearn.metrics import make_scorer, accuracy_score, precision_score, recall_score, f1_score
from sklearn.cross_validation import StratifiedShuffleSplit, StratifiedKFold
from sklearn import grid_search
from os.path import basename

if __name__ == "__main__":
    path = "F:\\Dropbox (Research)\\1 Thesis\\8. Experiment\\dataset\\temp\\a9a_scale.txt"
    rpath = "F:\\Dropbox (Research)\\1 Thesis\\8. Experiment\\result"
    X, y = load_svmlight_file(path)
    X = X.toarray()
    y[y != 1] = -1
    N = X.shape[0]
    bestAcc = 0; bestTime = 10e9; bestG = 0; bestC = 0;
    G = [2**(-5), 2**(-3), 2**(-1), 2**1, 2**3, 2**5]
    Lbd = [16.0/N, 4.0/N, 1.0/N, 1.0/(4*N), 1.0/(16*N)]
    bMaintenance = "removal"
    pjType = 1

    filename = basename(path)[:basename(path).find(".txt")]
    ofile = open(rpath + "\\" + filename + "_lmocBSGD_" + bMaintenance + "_" + str(pjType) + ".txt", "w")

    print "==== Dataset information ===="
    print "{} dimensions, {} vectors, {} positive, {} negative".format(X.shape[1], X.shape[0], len(y[y == 1]), len(y[y != 1]))
    print ""
    ofile.write("==== Dataset information ====\n")
    ofile.write("{} dimensions, {} vectors, {} positive, {} negative\n".format(X.shape[1], X.shape[0], len(y[y == 1]), len(y[y != 1])))
    ofile.write("\n")
    ofile.flush()

    skf = StratifiedKFold(y, n_folds=5, shuffle=False)
    for idx_train, idx_test in skf:
        X_train, y_train = X[idx_train], y[idx_train]
        X_test, y_test = X[idx_test], y[idx_test]

        """==========Grid Search=========="""
        for g_ in G:
            for lbd_ in Lbd:
                alg = lmocBSGD(T=100, gamma=g_, lamda=lbd_, B=50, bMaintenance=bMaintenance, pjType=pjType)
                alg.fit(X_train, y_train)
                y_pred = alg.predict(X_test)

                acc = alg.calcAccuaracy(y_pred, y_test)
                # acc = accuracy_score(y_test, y_pred)
                print "Gamma = {}, Lambda = {}, Acc = {:.4f}, Training time = {}".format(g_, lbd_, acc, alg.train_time_)
                ofile.write("Gamma = {}, Lambda = {}, Acc = {:.4f}, Training time = {}\n".format(g_, lbd_, acc, alg.train_time_))
                ofile.flush()

                if acc >= bestAcc:
                    bestAcc = acc
                    bestTime = alg.train_time_
                    bestG = g_
                    bestC = lbd_
        ofile.write("\n")

    print "======Final Result======"
    print "G = {}, Lambda = {}".format(bestG, bestC)
    print "Training time={:.4f}".format(bestTime)
    print "Accuracy={:.4f}".format(bestAcc)
    ofile.write("\n======Final Result======\n")
    ofile.write("G = {}, Lambda = {}\n".format(bestG, bestC))
    ofile.write("Training time={:.4f}\n".format(bestTime))
    ofile.write("Accuracy={:.4f}\n".format(bestAcc))

    ofile.close()