from __future__ import division

import time
import numpy as np
from pylearn.model.kernel_method.semiBSGD import semiBSGD
from sklearn.datasets import load_svmlight_file
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, make_scorer
from sklearn.cross_validation import StratifiedShuffleSplit, StratifiedKFold
from sklearn import grid_search
from pylearn.utils.util import utils
from os.path import basename

if __name__ == "__main__":
    """==========Parameters setting=========="""
    path = "G:\\Dropbox (Research)\\With Van\\WCCI2016\\BudgetedOnlineSemisupervisedSVM\\Experiment"
    trainingFilename = "svmguide3_training90.txt"
    testingFilename = "svmguide3_testing90.txt"
    bestAcc = 0; bestPrec = 0; bestRec = 0; bestF1 = 0; bestTime = 10e9; bestG = 0; bestC = 0;
    G = [2**(-5), 2**(-3), 2**(-1), 2**1, 2**3, 2**5]
    C = [2**(-5), 2**(-3), 2**(-1), 2**1, 2**3, 2**5]
    budgetType = "projection"
    projectionType = 0
    k_ = 4
    T_ = 50
    B1_ = 50
    B2_ = 50
    """==========End Parameters setting=========="""

    X_train, y_train = load_svmlight_file(path + "\\bdataset\\" + trainingFilename)
    X_train = X_train.toarray()
    X_test, y_test = load_svmlight_file(path + "\\bdataset\\" + testingFilename, n_features=X_train.shape[1])
    X_test = X_test.toarray()

    # filename = basename(path + "\\bdataset\\" + trainingFilename)[:basename(path + "\\bdataset\\" + trainingFilename).find(".txt_")]
    # if budgetType == "removal":
    #     tmp = ""
    # else:
    #     if projectionType == 0:
    #         tmp = "k-nn"
    #     else:
    #         tmp = "k-rnd"
    # ofile = open(path + "\\result\\" + filename + "_" + budgetType + "_" + tmp + ".txt", "w")

    print "==== Training Set information ===="
    print "{} dimensions, {} vectors, {} positive, {} negative".format(X_train.shape[1], X_train.shape[0], len(y_train[y_train == 1]), len(y_train[y_train != 1]))
    print "==== Testing Set information ===="
    print "{} dimensions, {} vectors, {} positive, {} negative".format(X_test.shape[1], X_test.shape[0], len(y_test[y_test == 1]), len(y_test[y_test != 1]))
    print ""
    # ofile.write("==== Training Set information ====\n")
    # ofile.write("{} dimensions, {} vectors, {} positive, {} negative\n".format(X_train.shape[1], X_train.shape[0], len(y_train[y_train == 1]), len(y_train[y_train != 1])))
    # ofile.write("==== Testing Set information ====\n")
    # ofile.write("{} dimensions, {} vectors, {} positive, {} negative\n\n".format(X_test.shape[1], X_test.shape[0], len(y_test[y_test == 1]), len(y_test[y_test != 1])))
    # ofile.flush()

    """==========Grid Search=========="""
    for g_ in G:
        for c_ in C:
            myAlg = semiBSGD(T=T_, bMaintenance=budgetType, k=k_, pjType=projectionType, B1=B1_, B2=B2_, C=c_, gamma=g_)
            myAlg.fit(X_train, y_train)
            y_pred = myAlg.predict(X_test)

            acc = accuracy_score(y_test, y_pred)
            print "G = {}, C = {}, Acc = {:.4f}, Training time = {}".format(g_, c_, acc, myAlg.train_time_)
            # ofile.write("G = {}, C = {}, Acc = {:.4f}, Training time = {:.4f}\n".format(g_, c_, acc, myAlg.train_time_))
            # ofile.flush()

            if acc > bestAcc:
                bestAcc = acc
                bestTime = myAlg.train_time_
                bestPrec = precision_score(y_test, y_pred)
                bestRec = recall_score(y_test, y_pred)
                bestF1 = f1_score(y_test, y_pred)
                bestG = g_
                bestC = c_
    """==========End Grid Search=========="""

    print "======Final Result======"
    print "G = {}, C = {}".format(bestG, bestC)
    print "Training time={:.4f}".format(bestTime)
    print "Accuracy={:.4f}, Precision={:.4f}, Recall={:.4f}, F1={:.4f}".format(bestAcc, bestPrec, bestRec, bestF1)
    # ofile.write("\n======Final Result======\n")
    # ofile.write("G = {}, C = {}\n".format(bestG, bestC))
    # ofile.write("Training time={:.4f}\n".format(bestTime))
    # ofile.write("Accuracy={:.4f}, Precision={:.4f}, Recall={:.4f}, F1={:.4f}\n".format(bestAcc, bestPrec, bestRec, bestF1))
    #
    # ofile.close()