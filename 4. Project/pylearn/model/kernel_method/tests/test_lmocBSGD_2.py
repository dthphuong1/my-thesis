from __future__ import division

import time
import numpy as np
from pylearn.model.kernel_method.lmocBSGD import lmocBSGD
from sklearn.datasets import load_svmlight_file
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, make_scorer
from sklearn.cross_validation import StratifiedShuffleSplit, StratifiedKFold
from sklearn import grid_search
from pylearn.utils.util import utils
from os.path import basename

if __name__ == "__main__":
    """==========Parameters setting=========="""
    path = "F:\\Dropbox (Research)\\1 Thesis\\8. Experiment\\dataset\\temp\\shuttle_scale.txt"
    rpath = "F:\\Dropbox (Research)\\1 Thesis\\8. Experiment\\result"
    X, y = load_svmlight_file(path)
    X = X.toarray()
    y[y != 1] = -1
    N = X.shape[0]
    fold = 0
    bestAcc = 0; bestPrec = 0; bestRec = 0; bestF1 = 0; bestTime = 10e9; bestG = 0; bestC = 0;
    G = [2**(-5), 2**(-3), 2**(-1), 2**1, 2**3, 2**5]
    Lbd = [16.0 / N, 4.0 / N, 1.0 / N, 1.0 / (4 * N), 1.0 / (16 * N)]
    BudgetList = [10,110,210,310,410,510,610,710,810,910,1010, 1600, 3100, 4600, 6100, 7600, 9100, 10600, 12100, 13600, 15100, 16600, 18100]
    budgetType = "removal"
    projectionType = 1
    k_ = 4
    T_ = 100
    """==========End Parameters setting=========="""

    skf = StratifiedKFold(y, n_folds=5, shuffle=False)
    for idx_train, idx_test in skf:
        X_train, y_train = X[idx_train], y[idx_train]
        X_test, y_test = X[idx_test], y[idx_test]
        fold += 1

        filename = basename(path)
        afile = open("F:\\Dropbox (Research)\\1 Thesis\\8. Experiment\\sresult\\" + filename + "_" + budgetType + "_" + str(fold) + "_Acc.txt", "w")
        tfile = open("F:\\Dropbox (Research)\\1 Thesis\\8. Experiment\\sresult\\" + filename + "_" + budgetType + "_" + str(fold) + "_Time.txt", "w")

        print "========================= Fold: {} =========================".format(fold)

        for b_ in BudgetList:
            myAlg = lmocBSGD(T=T_, bMaintenance=budgetType, k=k_, pjType=projectionType, B=b_, lamda=Lbd[4], gamma=G[4])
            myAlg.fit(X_train, y_train)
            y_pred = myAlg.predict(X_test)

            acc = myAlg.calcAccuaracy(y_pred, y_test)*100
            # acc = accuracy_score(y_test, y_pred)*100
            print "{}, {:.2f}, {:.2f}".format(b_, acc, myAlg.train_time_)
            afile.write("{:.2f}, ".format(acc)); tfile.write("{:.2f}, ".format(myAlg.train_time_))

    afile.close()
    tfile.close()