from __future__ import division

import time
import numpy as np
from pylearn.model.kernel_method.semiBSGD import semiBSGD
from sklearn.datasets import load_svmlight_file
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, make_scorer
from sklearn.cross_validation import StratifiedShuffleSplit, StratifiedKFold
from sklearn import grid_search
from pylearn.utils.util import utils
from os.path import basename

if __name__ == "__main__":
    """==========Parameters setting=========="""
    path = "G:\\Dropbox (Research)\\With Van\\WCCI2016\\BudgetedOnlineSemisupervisedSVM\\Experiment"
    trainingFilename = "dlg50c.txt_training90.txt"
    testingFilename = "dlg50c.txt_testing90.txt"
    bestAcc = 0; bestPrec = 0; bestRec = 0; bestF1 = 0; bestTime = 10e9; bestG = 0; bestC = 0;
    G = [2**(-5), 2**(-3), 2**(-1), 2**1, 2**3, 2**5]
    C = [2**(-5), 2**(-3), 2**(-1), 2**1, 2**3, 2**5]
    Bl = xrange(0, 50, 10)
    Bu = xrange(0, 400, 80)
    budgetType = "projection"
    projectionType = 0
    k_ = 4
    T_ = 20
    """==========End Parameters setting=========="""

    X_train, y_train = load_svmlight_file(path + "\\bdataset\\" + trainingFilename)
    X_train = X_train.toarray()
    X_test, y_test = load_svmlight_file(path + "\\bdataset\\" + testingFilename, n_features=X_train.shape[1])
    X_test = X_test.toarray()

    filename = basename(path + "\\bdataset\\" + trainingFilename)[:basename(path + "\\bdataset\\" + trainingFilename).find(".txt_")]
    afile = open(path + "\\result_budget\\" + filename + "_" + budgetType + "_Acc.txt", "w")
    tfile = open(path + "\\result_budget\\" + filename + "_" + budgetType + "_Time.txt", "w")

    print "==== Training Set information ===="
    print "{} dimensions, {} vectors, {} Labeled, {} Unlabeled".format(X_train.shape[1], X_train.shape[0], len(y_train[y_train != -3]), len(y_train[y_train == -3]))
    print "==== Testing Set information ===="
    print "{} dimensions, {} vectors, {} Labeled, {} Unlabeled".format(X_test.shape[1], X_test.shape[0], len(y_test[y_test != -3]), len(y_test[y_test == -3]))
    print ""

    for Bl_ in Bl:
        for Bu_ in Bu:
            myAlg = semiBSGD(T=T_, bMaintenance=budgetType, k=k_, pjType=projectionType, B1=Bl_, B2=Bu_, C=C[1], gamma=G[2])
            myAlg.fit(X_train, y_train)
            y_pred = myAlg.predict(X_test)

            acc = accuracy_score(y_test, y_pred)*100
            print "{}, {}, {:.2f}, {:.2f}".format(Bl_, Bu_, acc, myAlg.train_time_)
            afile.write("{:.2f}, ".format(acc)); tfile.write("{:.2f}, ".format(myAlg.train_time_))
    afile.close()
    tfile.close()