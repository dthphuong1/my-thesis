"""Budget Semi-supervised Support Vector Machine
"""

from __future__ import division

import time
from pylearn.model.kernel_method.sgd import KSGD
import numpy as np
from sklearn.base import BaseEstimator
from sklearn.metrics import accuracy_score, mean_squared_error
from sklearn.neighbors import NearestNeighbors
from sklearn.metrics.pairwise import rbf_kernel

LOSS = {"hinge": 1, "l1": 2, "l2": 3, "logit": 4, "eps_intensive": 5}
TASK = {"classification": 1, "regression": 2}
KERNEL = {"gaussian": 1}
BUDGET_MAINTENANCE = {"removal": 1, "projection": 2}
EPS = 10e-4

class semiBSGD(KSGD):

    def __init__(self, loss="hinge", eps=0.1,
                 kernel="gaussian", gamma=0.1,
                 lbd=1.0, avg_weight=False, verbose=0, T=0,
                 C=1.0, B1=500, B2=500,
                 bMaintenance="removal", k=4, pjType=0):
        self.loss = loss
        self.eps = eps
        self.kernel = kernel
        self.gamma = gamma
        self.lbd = lbd
        self.avg_weight = avg_weight
        self.verbose = verbose
        self.T = T
        self.C = C
        self.B1 = B1
        self.B2 = B2
        self.k_ = k
        self.bMaintenance = bMaintenance
        self.projectionType = pjType

    def init(self):
        try:
            self.loss = LOSS[self.loss]
        except KeyError:
            raise ValueError("Loss function %s is not supported." % self.loss)

        try:
            self.kernel = KERNEL[self.kernel]
        except KeyError:
            raise ValueError("Kernel %s is not supported." % self.kernel)

        try:
            self.bMaintenance = BUDGET_MAINTENANCE[self.bMaintenance]
        except KeyError:
            raise ValueError("Budget Maintenance %s is not supported" % self.bMaintenance)

        if self.loss == LOSS["hinge"] or self.loss == LOSS["logit"]:
            self.task_ = TASK["classification"]
        else:
            self.task_ = TASK["regression"]

        self.n_classes_ = 0
        self.class_name_ = None
        self.idw_ = None
        self.w_ = None
        self.X_ = None
        self.y_ = None
        self.train_time_ = 0
        self.w_avg = None
        self.lSet = None
        self.uSet = None
        self.dSet = None

    def get_wx(self, t, x):
        if t == 0:
            return [0]
        else:
            if self.kernel == KERNEL["gaussian"]:
                xx = (self.X_[:t, :] - x)
                return np.sum(self.w_[:t, :]*np.exp(-self.gamma*np.sum(xx*xx, axis=1, keepdims=True)), axis=0)
            else:
                return [0]

    def get_wxy(self, t, x, y):
        if t == 0:
            return (0, -1)
        else:
            if self.kernel == KERNEL["gaussian"]:
                xx = (self.X_[:t, :]-x)
                k = np.sum(self.w_[:t, :]*np.exp(-self.gamma*np.sum(xx*xx, axis=1, keepdims=True)), axis=0)
                idx = np.ones(self.n_classes_, np.bool)
                idx[y] = False
                z = np.argmax(k[idx])
                z += (z >= y)
                return (k[y] - k[z], z)
            else:
                return (0, -1)

    def get_grad(self, t, x, y):
        if self.n_classes_ > 2:
            wxy, z = self.get_wxy(t, x, y)
            if self.loss == LOSS["hinge"]:
                return (-1, z) if wxy <= 1 else (0, z)
            else:
                return (-1 / (1 + np.exp(wxy)), z)
        else:
            wx = self.get_wx(t, x)[0]
            if self.loss == LOSS["hinge"]:
                return (-y, -1) if y*wx <= 1 else (0, -1)
            elif self.loss == LOSS["l1"]:
                return (np.sign(wx - y), -1)
            elif self.loss == LOSS["l2"]:
                return (y*(y*wx - 1), -1)
            elif self.loss == LOSS["logit"]:
                return (-y*np.exp(-y*wx) / (np.exp(-y*wx) + 1), -1)
            elif self.loss == LOSS["eps_intensive"]:
                return (np.sign(wx - y), -1) if np.abs(y - wx) > self.eps else (0, -1)

    def BudgetMaintenance(self, isLabeled):
        N = self.X_.shape[0]

        """============Budget Maintenance==========="""
        if isLabeled:
            w = self.w_[self.lSet]
            id = np.argmin(np.abs(w))
            p = self.lSet[id]
        else:
            # print self.uSet
            # print self.w_
            w = self.w_[self.uSet]
            id = np.argmin(np.abs(w))
            p = self.uSet[id]

        if self.bMaintenance == BUDGET_MAINTENANCE["projection"]:
            if self.projectionType == 0: #k-nn algorithm
                nn = NearestNeighbors(n_neighbors=self.k_ + 1)
                nn.fit(self.X_)
                knn = nn.kneighbors(self.X_[p], return_distance=False)[0]
                knn = np.delete(knn, 0, axis=0)
            else: #random k SVs algorithm
                knn = np.random.choice(N, self.k_, replace=False)

            X_knn = self.X_[knn];
            xp = self.X_[p]
            P = rbf_kernel(xp, X_knn, gamma=self.gamma).T
            Q = rbf_kernel(X_knn, X_knn, gamma=self.gamma)
            tQ = (Q == 1)
            np.fill_diagonal(tQ, False)
            Q[tQ] -= EPS
            dAlpha = np.linalg.solve(Q, P)
            dAlpha *= self.w_[p]
            self.w_[knn] *= dAlpha

        self.dSet[p] = p
        self.w_ = np.delete(self.w_, p, axis=0)
        self.X_ = np.delete(self.X_, p, axis=0)
        self.y_ = np.delete(self.y_, p, axis=0)
        if self.avg_weight:
            self.w_avg = np.delete(self.w_avg, p, axis=0)
        """===========End Budget Maintenance==========="""

    def fit(self, X, y):
        start_time = time.time()

        self.init()

        N = X.shape[0]
        if self.task_ == TASK["classification"]:
            self.class_name_ = np.unique(y)
            self.class_name_ = np.delete(self.class_name_, np.where(self.class_name_ == -3))
            self.n_classes_ = len(self.class_name_)
            if self.n_classes_ == 2:
                y[y == 0] = -1

        if self.n_classes_ > 2:
            self.w_ = np.zeros((N, self.n_classes_))
        else:
            self.w_ = np.zeros((N, 1))
            self.idw_ = np.ones((N,1)) * (-1)
            self.dSet = np.ones((N,1)) * (-1)

        if self.avg_weight:
            w_avg = np.zeros(self.w_.shape)

        self.X_ = X
        self.y_ = y
        lB = 0; uB = 0; t = 0; T = N

        for t in xrange(self.T):

            self.lSet = np.where(self.y_ != -3)[0]
            self.uSet = np.where(self.y_ == -3)[0]

            it = self.lSet[np.random.randint(0, self.lSet.shape[0],1)]
            ut = self.lSet[np.random.randint(0, self.lSet.shape[0],1)]
            vt = self.uSet[np.random.randint(0, self.uSet.shape[0],1)]
            muy = np.exp(-self.gamma*np.sum((X[ut]-X[vt])*(X[ut]-X[vt])))

            alpha_it = self.get_grad(it, X[it], self.y_[it])[0] * (self.C / (t+1))
            alpha_vt = (2*self.C/(t+1)) * self.get_wx(vt, X[vt])[0] * muy
            alpha_ut = (2*self.C/(t+1)) * self.get_wx(ut, X[ut])[0] * muy

            self.w_ *= (1.0*t)/(t+1)
            if it not in self.idw_: #New vector
                self.w_[it] = -alpha_it
                self.idw_[it] = it
                lB += 1
            else:
                self.w_[it] += -alpha_it

            if ut not in self.idw_: #New vector
                self.w_[ut] = alpha_vt - alpha_ut
                self.idw_[ut] = ut
                lB += 1
            else:
                self.w_[ut] += (alpha_vt - alpha_ut)

            if vt not in self.idw_: #New vector
                self.w_[vt] = alpha_ut - alpha_vt
                self.idw_[vt] = vt
                uB += 1
            else:
                self.w_[vt] += (alpha_ut - alpha_vt)

            """Check to Run Budget Maintenance"""
            if lB > self.B1:
                self.BudgetMaintenance(isLabeled=True)
                lB -= 1

            if uB > self.B2:
                self.BudgetMaintenance(isLabeled=False)
                uB -= 1
            """============EndCheck==========="""

            if self.avg_weight:
                self.w_avg += self.w_

        if self.avg_weight:
            self.w_ = self.w_avg / N

        self.train_time_ = time.time() - start_time

    def predict(self, X):
        y = np.zeros(X.shape[0])
        for i in xrange(X.shape[0]):
            wx = self.get_wx(len(self.w_), X[i])
            if self.task_ == TASK["classification"]:
                y[i] = self.class_name_[wx[0] >= 0]
            else:
                y[i] = wx[0]
        return y