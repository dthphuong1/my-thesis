"""Large Margin One-class BSGD
"""

from __future__ import division

import time
from pylearn.model.kernel_method.sgd import KSGD
import numpy as np
from sklearn.base import BaseEstimator
from sklearn.metrics import accuracy_score, mean_squared_error
from sklearn.neighbors import NearestNeighbors
from sklearn.metrics.pairwise import rbf_kernel

LOSS = {"hinge": 1, "l1": 2, "l2": 3, "logit": 4, "eps_intensive": 5}
TASK = {"classification": 1, "regression": 2}
KERNEL = {"gaussian": 1}
BUDGET_MAINTENANCE = {"removal": 1, "projection": 2}
EPS = 10e-4

class lmocBSGD(KSGD):

    def __init__(self, loss="hinge", eps=0.1,
                 kernel="gaussian", gamma=0.1, lamda=1.0,
                 avg_weight=False, verbose=0, T=0,
                 B=500, bMaintenance="removal", k=4, pjType=0):
        self.loss = loss
        self.eps = eps
        self.kernel = kernel
        self.gamma = gamma
        self.avg_weight = avg_weight
        self.verbose = verbose
        self.T = T
        self.lamda = lamda
        self.B = B
        self.k_ = k
        self.bMaintenance = bMaintenance
        self.projectionType = pjType

    def init(self):
        try:
            self.loss = LOSS[self.loss]
        except KeyError:
            raise ValueError("Loss function %s is not supported." % self.loss)

        try:
            self.kernel = KERNEL[self.kernel]
        except KeyError:
            raise ValueError("Kernel %s is not supported." % self.kernel)

        try:
            self.bMaintenance = BUDGET_MAINTENANCE[self.bMaintenance]
        except KeyError:
            raise ValueError("Budget Maintenance %s is not supported" % self.bMaintenance)

        if self.loss == LOSS["hinge"] or self.loss == LOSS["logit"]:
            self.task_ = TASK["classification"]
        else:
            self.task_ = TASK["regression"]

        self.n_classes_ = 0
        self.class_name_ = None
        self.idw_ = None
        self.w_ = None
        self.X_ = None
        self.y_ = None
        self.train_time_ = 0
        self.w_avg = None
        self.dSet = None

    def get_grad(self, t, x, y):
        if self.n_classes_ > 2:
            wxy, z = self.get_wxy(t, x, y)
            if self.loss == LOSS["hinge"]:
                return (-1, z) if wxy <= 1 else (0, z)
            else:
                return (-1 / (1 + np.exp(wxy)), z)
        else:
            wx = self.get_wx(t, x)[0]
            denta = (1 - y)/2
            if self.loss == LOSS["hinge"]:
                return (-y, -1) if y*wx <= denta else (0, -1)
            elif self.loss == LOSS["l1"]:
                return (np.sign(wx - y), -1)
            elif self.loss == LOSS["l2"]:
                return (y*(y*wx - 1), -1)
            elif self.loss == LOSS["logit"]:
                return (-y*np.exp(-y*wx) / (np.exp(-y*wx) + 1), -1)
            elif self.loss == LOSS["eps_intensive"]:
                return (np.sign(wx - y), -1) if np.abs(y - wx) > self.eps else (0, -1)

    def BudgetMaintenance(self):
        N = self.X_.shape[0]

        """============Budget Maintenance==========="""
        p = np.argmin(np.abs(self.w_))

        if self.bMaintenance == BUDGET_MAINTENANCE["projection"]:
            if self.projectionType == 0: #k-nn algorithm
                nn = NearestNeighbors(n_neighbors=self.k_ + 1)
                nn.fit(self.X_)
                dis, knn = nn.kneighbors(self.X_[p], return_distance=True)
                knn = knn[0]
                dis = dis[0]
                knn = np.delete(knn, 0, axis=0)
                dis = np.delete(dis, 0, axis=0)
            else: #random k SVs algorithm
                knn = np.random.choice(N, self.k_, replace=False)

            X_knn = self.X_[knn];
            xp = self.X_[p]
            P = rbf_kernel(xp, X_knn, gamma=self.gamma).T
            Q = rbf_kernel(X_knn, X_knn, gamma=self.gamma)
            tQ = (Q == 1)
            np.fill_diagonal(tQ, False)
            Q[tQ] -= EPS
            dAlpha = np.linalg.solve(Q, P)
            dAlpha *= self.w_[p]
            self.w_[knn] *= dAlpha

        self.dSet[p] = p
        self.w_ = np.delete(self.w_, p, axis=0)
        self.X_ = np.delete(self.X_, p, axis=0)
        self.y_ = np.delete(self.y_, p, axis=0)
        if self.avg_weight:
            self.w_avg = np.delete(self.w_avg, p, axis=0)
        """===========End Budget Maintenance==========="""

    def fit(self, X, y):
        start_time = time.time()

        self.init()

        N = X.shape[0]
        if self.task_ == TASK["classification"]:
            self.class_name_ = np.unique(y)
            # self.class_name_ = np.delete(self.class_name_, np.where(self.class_name_ == -3))
            self.n_classes_ = len(self.class_name_)
            if self.n_classes_ == 2:
                y[y == 0] = -1

        if self.n_classes_ > 2:
            self.w_ = np.zeros((N, self.n_classes_))
        else:
            self.w_ = np.zeros((N, 1))
            self.idw_ = np.ones((N,1)) * (-1)
            self.dSet = np.ones((N,1)) * (-1)

        if self.avg_weight:
            w_avg = np.zeros(self.w_.shape)

        self.X_ = X
        self.y_ = y
        b = 0; t = 0; T = N

        for t in xrange(self.T):
            it = np.random.randint(0, self.X_.shape[0],1)
            alpha_it = self.get_grad(it, X[it], self.y_[it])[0] * (self.lamda / (t+1))
            self.w_ *= (1.0*t)/(t+1)

            if it not in self.idw_: #New vector
                self.w_[it] = -alpha_it
                self.idw_[it] = it
                b += 1
            else:
                self.w_[it] += -alpha_it

            """Check to Run Budget Maintenance"""
            if b > self.B:
                self.BudgetMaintenance()
                b -= 1
            """============EndCheck==========="""

            if self.avg_weight:
                self.w_avg += self.w_

        if self.avg_weight:
            self.w_ = self.w_avg / N

        self.train_time_ = time.time() - start_time

    def predict(self, X):
        y = np.zeros(X.shape[0])
        for i in xrange(X.shape[0]):
            wx = self.get_wx(len(self.w_), X[i])
            if self.task_ == TASK["classification"]:
                y[i] = self.class_name_[wx[0] >= 0]
            else:
                y[i] = wx[0]
        return y

    def calcAccuaracy(self, yPredict, yTrue):
        acc = 0; acc_p = 0; acc_n = 0;
        n = yTrue.shape[0]
        nrPos = np.where(yTrue == 1)[0].shape[0]
        nrNeg = np.where(yTrue == -1)[0].shape[0]

        for i in xrange(n):
            if (yPredict[i] == yTrue[i]):
                if (yTrue[i] == 1):
                    acc_p += 1
                else:
                    acc_n += 1

        return (acc_p/nrPos + acc_n/nrNeg)/2