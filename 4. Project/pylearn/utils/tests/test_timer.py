from pylearn.utils import Timer
from pylearn.model.deep_learning.rbm import BernoulliRBM
from sklearn.cross_validation import StratifiedShuffleSplit


if __name__ == '__main__':
    from sklearn.datasets import fetch_mldata
    mnist = fetch_mldata('MNIST original', data_home="/Users/tund/tund/project/sklearn_data")

    train_idx, test_idx = next(iter(StratifiedShuffleSplit(mnist.target, 1, test_size=10000, random_state=6789)))
    X_train0 = mnist.data[train_idx, :] / 255.0
    X_test0 = mnist.data[train_idx, :] / 255.0

    X_train = X_train0[:1000, :]

    rbm_inst = BernoulliRBM(n_hid=500, max_iter=10, random_state=6789)

    with Timer() as t:
        rbm_inst.fit(X_train)
    print "=> elasped rbm.fit(): %s s" % t.secs
