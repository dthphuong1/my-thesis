import numpy as np

from pylearn.utils import pdist

if __name__ == '__main__':
    X = np.random.rand(3, 4)
    Y = np.random.rand(5, 4)
    print pdist(X, Y)