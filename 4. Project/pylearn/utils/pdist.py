"""
Compute pairwise distance
"""

import numpy as np
import numpy.linalg as LA

EPS = np.finfo(np.float).eps

def pdist(X, Y, dist_type="cosine"):
    if dist_type == "cosine":
        X_norm = LA.norm(X, axis=1).reshape((1, -1))
        Y_norm = LA.norm(Y, axis=1).reshape((-1, 1))
        return -(Y.dot(X.T) / (EPS + Y_norm.dot(X_norm)))
    else:
        print "Distance type %s is not supported yet!" % dist_type
        return -1