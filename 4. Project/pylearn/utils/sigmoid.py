"""Compute sigmoid function: y = 1 / (1 + exp(-x))
"""

import numpy as np


def sigmoid(x):
    max0 = np.maximum(x, 0)
    return np.exp(x-max0) / (np.exp(x-max0) + np.exp(-max0))
