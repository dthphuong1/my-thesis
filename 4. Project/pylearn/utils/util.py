import numpy as np
from sklearn.cluster import KMeans

class utils:
    def __init__(self, nrCluster=5, percent=0.5, type=0):
        self.nrCluster = nrCluster
        self.percent = percent
        self.type = type

    def make_unlabeled(self, X, y, percent=0.5, type=0):
        N = X.shape[0]
        m = N*percent

        if type == 0: # Random
            uid = np.random.choice(N, size=m, replace=False)
            y[uid] = 0
        else: # Clustering
            kmean = KMeans(n_clusters=self.nrCluster)
            kmean.fit(X,y)
            kLabel = kmean.labels_

            for i in xrange(self.nrCluster):
                yy = np.where(kLabel == i)
                nn = yy[0].shape[0]
                mm = nn*percent + 1
                uid = np.random.choice(nn, size=mm, replace=False)
                y[uid] = 0
        return y