#include <eHealth.h>
#include <eHealthDisplay.h>

// The setup routine runs once when you press reset:
void setup() {
  Serial.begin(115200);
}

// The loop routine runs over and over again forever:
void loop() {
  float ECG = eHealth.getECG();

  Serial.println(ECG, 2);

  delay(100);	// wait for a millisecond
}
                
