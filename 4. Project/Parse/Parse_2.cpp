#include <bits/stdc++.h>
using namespace std;

int main() {
	double signal;
	ifstream ifile("ecg2.txt");
	ofstream ofile("code2.txt");
	
	double time = 0;
	while (!ifile.eof()) {
		ifile >> signal;
		ofile << time << " " << signal << "\n";
		time += (double)0.8/289;
	}
	
	ofile.close();
	ifile.close();
	
	return 0;
}
