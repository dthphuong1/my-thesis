I. Mở đầu
	1. Bài toán dò bất thường
		- Tổng quan + ý tưởng
		- Một số PP đã có 
		- Ứng dụng trong thực tế
		
		# Chuyển ý: độ phức tạp cao (thường lớn hơn O(n^2)) --> k phù hợp với dữ liệu lớn và cản trở bài toán dò bất thường trong thực tế
		
	2. Stochastic Gradient Descent
		- Tổng quan + các điểm nổi bật --> kết hợp Kernel và SGD
		- Một số PP nổi bật: SGD-LMOC 
		
		# Chuyển ý: Mặc dù SGD-LMOC học tốt và phù hợp với dữ liệu lớn --> tình trạng Curse of Kernelization --> thời gian tính toán chậm + chiếm bộ nhớ --> tràn bộ nhớ trong tương lai

II. Budgeted Stochastic Gradient Descent Large Margin One-class
	3. BSGD-LMOC (đóng góp đầu tiên trong Khoá luận)
		- Ý tưởng (nên vẽ hình để dễ hình dung)
		- Tốc độ hội tụ + Budget Maintenance (Nói 2 PP k nói chi tiết - nói chi tiết ở phía sau)
		- Thực nghiệm (Thực nghiệm trên dữ liệu UCI + Thực nghiệm thể hiện tính hội tụ)
		
		# Chuyển ý: Chuyển trực tiếp qua phần 4

III. Semi supervised-learning + Spectral graph
	4. Semi supervised-learning + Spectral graph
		- Intro
			+ Dữ liệu trong cuộc sống (tăng nhanh, vây quanh bởi dữ liệu, . . . .)
			+ Dữ liệu k nhãn nhiều --> chi phí gán nhãn + cần chuyên gia chuyên ngành
			+ Dữ liệu dù là k nhãn cũng chứa thông tin về phân bố của nó --> nên tận dụng để nâng cao độ chính xác của việc học
		- Semi supervised-learning
			+ Tổng quan + ý tưởng
		- Spectral graph
			+ Tổng quan + ý tưởng + hình ảnh minh hoạ
		
		# Chuyển ý: Do những yếu tố trên, Semi supervised-learning + Spectral graph được cộng đồng ML quan tâm
	
IV. Budgeted Semi supervised Large Margin One-class
	5. BS2LMOC (đóng góp thứ hai trong Khoá luận)
		- Ý tưởng (nên vẽ hình để dễ hình dung)
		- Cách lan truyền nhãn
		- Tốc độ hội tụ + Budget Maintenance (Nói 2 PP k nói chi tiết - nói chi tiết ở phía sau)
		- Thực nghiệm (Thực nghiệm trên dữ liệu UCI + Thực nghiệm thể hiện tính hội tụ)
	
V. Thiết bị phát hiện mệt mỏi bằng điện tâm đồ ECG
	6. Fatigue Detected Device - FDD (đóng góp thứ ba trong Khoá luận - đóng góp mang tính ứng dụng thực tế)
		- Intro: Để chứng minh khả năng áp dụng trong thực tiễn của các thuật toán được đề xuất . . . .
		- Ý tưởng của thiết bị: do đâu mà có ? học như thế nào ?
		- Cấu tạo của thiết bị + Nguyên lý hoạt động
		- DEMO thực nghiệm: Video + LIVE (if need)

VI. Kết quả
	- Qua khoá luận này, chúng tôi đã đóng góp đc: 
		1. BSGD-LMOC: học tốt trên bài toán Novelty Detection + cho kq thực nghiệm tốt + độ hội tụ tốt
		2. BS2LMOC: học tốt trên bài toán Novelty Detection trong ngữ cảnh học bán giám sát + cho kq thực nghiệm tốt + độ hội tụ tốt
		3. Thiết bị FDD: thiết bị có khả năng học tốt trên dữ liệu ECG và phát hiện ra mệt mỏi